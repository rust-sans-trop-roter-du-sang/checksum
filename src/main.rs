use std::env;
use std::path::Path;

fn main() {
    for arg in env::args().skip(1) {
        match sha256::try_digest(Path::new(&arg)) {
            Ok(sum) => println!("{} {}", arg, sum),
            Err(error) => eprintln!("{} {}", arg, error),
        }
    }
}
